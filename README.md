Zamiast readme:

Obrazy scen pochodzą z filmu, który nakręciłem podczas rowerowej wycieczki a który znajduje sie tu: https://youtu.be/Sr9sv7gYCso . Film, z braku lepszego pomysłu, pociąłem na kletki wg instrukcji z filmu https://www.youtube.com/watch?v=NIzWZg02kHU.

Tu, wydaje się, jest kilka lepszych pomysłów;
 - https://superuser.com/questions/135117/how-to-extract-one-frame-of-a-video-every-n-seconds-to-an-image
 - https://stackoverflow.com/questions/10225403/how-can-i-extract-a-good-quality-jpeg-image-from-a-video-file-with-ffmpeg

------------

Schemat scen znajduje się w lokalizacji `frames/` (jest to plik dia http://dia-installer.de/ (starożytny, ale generuje czytelne pliki XML).


---


TODO:
 - `20_generate_scenes_from_dia_file.py` does not generate basic `.gd` files and does not add the file as script for scene. That should be easy to add
 - There are some formatting issues when opening generated scene with Godot. Maybe there is a way to open/save these at the very end of generation process?

VARIA:
 - maybe use chess figures instead of pearls? https://www.hiclipart.com/free-transparent-background-png-clipart-ymmpq
 - 