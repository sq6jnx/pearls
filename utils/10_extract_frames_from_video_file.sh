#!/bin/bash

set -eu

INPUT_FILE="../video/20231231_124210.mp4"
OUTPUT_PATH="../video"

ffmpeg -i "${INPUT_FILE}" -qscale:v 2 -r 1 "${OUTPUT_PATH}/frame%05d.jpg"

# rename files to keep names consistent with previous ones (made by VLC).
# It looks like ffmpeg dumps first frame twice and numbering goes off
# when compared to VLC. Well...
rm "${OUTPUT_PATH}/frame00001.jpg"
for f in "${OUTPUT_PATH}"/*jpg; do
  old_num=$(echo "${f}" | sed 's/[^0-9A-Z]*//g;s/^0*//g')
  if [[ $old_num == 1 ]]; then continue ; fi
  new_num=$(((old_num - 1) * 30 + 1))
  mv "${f}" ${OUTPUT_PATH}/scene$(printf "%05d" "${new_num}").jpg
done