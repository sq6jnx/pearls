import collections
import pathlib
import shutil
import typing

import godot_parser as gp
import lxml.etree


DIA_FILE = pathlib.Path(__file__).parent.parent / "video" / "diagram.dia"
IMAGES_PATH = DIA_FILE.parent
NS = {"dia": "http://www.lysator.liu.se/~alla/dia/"}
GODOT_TEMPLATE = (
    pathlib.Path(__file__).parent.parent / "godot" / "templates" / "template.tscn"
)
SCENE_OUT_PATH = GODOT_TEMPLATE.parent.parent / "scenes"

DiaScene = collections.namedtuple(
    "DiaScene",
    [
        "scene_id",
        "image_file",
    ],
)


def get_scenes(tree: lxml.etree.ElementTree):
    """Check whether:
    - every image node contains path to file and that file
    - aforementioned file exists
    - every image node contains its ID
    - all IDs are unique
    """
    retval: typing.List[DiaScene] = []
    scene_ids_found = set()
    images_used = set()

    for e in tree.findall(
        'dia:layer/dia:object[@type="Standard - Image"]', namespaces=NS
    ):
        dia_id = e.find(
            './dia:attribute[@name="meta"]/dia:composite[@type="dict"]/dia:attribute[@name="id"]/dia:string',
            namespaces=NS,
        )

        if dia_id is None or dia_id.text == "":
            raise ValueError(
                "Scene is missing its id: " + lxml.etree.tostring(e).decode()
            )
        scene_id = dia_id.text[1:-1]

        dia_filename = e.find(
            './dia:attribute[@name="file"]/dia:string', namespaces=NS
        ).text

        image_file = IMAGES_PATH / dia_filename[1:-1]
        if not image_file.exists():
            raise ValueError("File {} does not exist".format(image_file))

        if scene_id in scene_ids_found:
            raise ValueError("Duplicate scene id: " + scene_id)
        if image_file in images_used:
            print(
                "WARNING: Image used multiple times (here in node {}): {}".format(
                    scene_id, image_file
                )
            )
        scene_ids_found.add(scene_id)
        images_used.add(image_file)
        retval.append(DiaScene(scene_id=scene_id, image_file=image_file))
    return retval


def make_scene(dia_scene: DiaScene):
    scene_filename = "scene_{}.tscn".format(dia_scene.scene_id)
    output_filename = SCENE_OUT_PATH / scene_filename
    if output_filename.exists():
        scene = gp.load(output_filename)
    else:
        scene = gp.load(GODOT_TEMPLATE)

    jpg_filename = "scene_{}.jpg".format(dia_scene.scene_id)

    # Add *NEW* background resource
    background_resource = scene.add_ext_resource(
        "res://scenes/" + jpg_filename, type="Texture2D"
    )

    root = scene.find_node(name="Template")
    root.name = "Scene" + dia_scene.scene_id

    background = scene.find_node(name="Background")
    background.properties = {
        **background.properties,
        "texture": gp.ExtResource(background_resource.id),
    }

    scene.remove_unused_resources()
    scene.renumber_resource_ids()

    shutil.copy(dia_scene.image_file, SCENE_OUT_PATH / jpg_filename)
    scene.write(output_filename)


def main():
    tree = lxml.etree.parse(DIA_FILE)
    scenes = get_scenes(tree)
    for scene in scenes:
        if scene.scene_id == 'intro':
            make_scene(scene)


if __name__ == "__main__":
    main()
