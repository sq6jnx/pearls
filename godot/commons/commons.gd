extends Node

const FIRST_ROUND_PEARLS = 10
const SECOND_ROUND_PEARLS = 2

var score: int = 0

func get_main():
	return get_parent().get_child(2)


func change_scene(new_scene: Scenes.Scene):
	get_main().change_scene(new_scene)


func pearl_taken():
	get_main().pearl_taken()


func start_over():
	get_tree().change_scene_to_file("res://menus/welcome.tscn")
