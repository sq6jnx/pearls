extends Node2D

enum Scene {
	NO_SCENE,
	S0100a2,
	S0100a,
	S0100,
	S0300a,
	S0300,
	S0500,
	S0600a2,
	S0600a3,
	S0600a,
	S0600,
	S0700a,
	S0700,
	S0900a2,
	S0900a,
	S0900b,
	S0900c,
	S0900,
	S1200a,
	S1200a2,
	S1200,
	intro,
	game_over
}

var scenes: Dictionary = {}


func _init():
	scenes = {
		Scene.S0100a2: load("res://scenes/scene_0100a2.tscn").instantiate(),
		Scene.S0100a: load("res://scenes/scene_0100a.tscn").instantiate(),
		Scene.S0100: load("res://scenes/scene_0100.tscn").instantiate(),
		Scene.S0300a: load("res://scenes/scene_0300a.tscn").instantiate(),
		Scene.S0300: load("res://scenes/scene_0300.tscn").instantiate(),
		Scene.S0500: load("res://scenes/scene_0500.tscn").instantiate(),
		Scene.S0600a2: load("res://scenes/scene_0600a2.tscn").instantiate(),
		Scene.S0600a3: load("res://scenes/scene_0600a3.tscn").instantiate(),
		Scene.S0600a: load("res://scenes/scene_0600a.tscn").instantiate(),
		Scene.S0600: load("res://scenes/scene_0600.tscn").instantiate(),
		Scene.S0700a: load("res://scenes/scene_0700a.tscn").instantiate(),
		Scene.S0700: load("res://scenes/scene_0700.tscn").instantiate(),
		Scene.S0900a2: load("res://scenes/scene_0900a2.tscn").instantiate(),
		Scene.S0900a: load("res://scenes/scene_0900a.tscn").instantiate(),
		Scene.S0900b: load("res://scenes/scene_0900b.tscn").instantiate(),
		Scene.S0900c: load("res://scenes/scene_0900c.tscn").instantiate(),
		Scene.S0900: load("res://scenes/scene_0900.tscn").instantiate(),
		Scene.S1200a: load("res://scenes/scene_1200a.tscn").instantiate(),
		Scene.S1200a2: load("res://scenes/scene_1200a2.tscn").instantiate(),
		Scene.S1200: load("res://scenes/scene_1200.tscn").instantiate(),
		Scene.intro: load("res://scenes/intro.tscn").instantiate(),
		Scene.game_over: load("res://scenes/game_over.tscn").instantiate(),
		}
