extends Area2D

@export var go_to_node: Scenes.Scene = Scenes.Scene.NO_SCENE:
	get:
		return go_to_node
	set(value):
		go_to_node = value
		visible = (value != Scenes.Scene.NO_SCENE)
		
func _ready():
	visible = (go_to_node != Scenes.Scene.NO_SCENE)

func _on_mouse_entered():
	Input.set_default_cursor_shape(Input.CURSOR_POINTING_HAND)

func _on_mouse_exited():
	Input.set_default_cursor_shape(Input.CURSOR_ARROW)

func _on_input_event(_viewport, event, _shape_idx):
	if (
		go_to_node != Scenes.Scene.NO_SCENE
		and event is InputEventMouseButton
		and event.button_index == MOUSE_BUTTON_LEFT
		and event.is_pressed()):
		Commons.change_scene(go_to_node)
