extends Node2D

const SCENE_LEFT = Scenes.Scene.NO_SCENE
const SCENE_RIGHT = Scenes.Scene.NO_SCENE
const SCENE_BACK = Scenes.Scene.NO_SCENE

func _process(_delta):
	if Commons.score == Commons.FIRST_ROUND_PEARLS + Commons.SECOND_ROUND_PEARLS:
		$Summary.text="""[center][b]Congratulations!:[/b]
You managed to find all the pearls!

If you want to start over just go back this path."""
	else:
		$Summary.text="""[center][b]No luck this time!:[/b]
You did not managed to find all the pearls!

If you want to start over just go back this path."""


func _on_navigation_region_input_event(_viewport, event, _shape_idx):
	if (
		event is InputEventMouseButton
		and event.button_index == MOUSE_BUTTON_LEFT
		and event.is_pressed()):
		Commons.start_over()
