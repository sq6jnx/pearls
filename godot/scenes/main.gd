extends Node2D

const START_TIME = 600 # 10 minutes, in seconds
const START_SCENE = Scenes.Scene.intro

var time_left: float = 0.
var pearls: Array = []
var second_round_started: bool = false


func _ready():
	Scenes._init()
	prepare_game()
	change_scene(START_SCENE)

func prepare_game():
	# Collect all pearl nodes
	for key in Scenes.scenes.keys():
		var scene: Node2D = Scenes.scenes[key]
		pearls += scene.get_node("Pearls").get_children()
	
	# Shuffle
	pearls.shuffle()
	
	# Make first 2 pearls visible, hide others
	for i in pearls.size():
		print("Pearl in " + str(pearls[i].get_parent().get_parent()))
		pearls[i].visible = (i < Commons.FIRST_ROUND_PEARLS)
	Commons.score = 0
	time_left = START_TIME
	second_round_started = false



func change_scene(new_scene: Scenes.Scene):
	if $CurrentScene.get_child_count():
		$CurrentScene.remove_child($CurrentScene.get_child(0))
	var instance: Node2D = Scenes.scenes[new_scene]
	$CurrentScene.add_child(instance)
	print("Changed scene to " + str(instance))
	$Navigation/GoBack.go_to_node = instance.SCENE_BACK
	$Navigation/GoLeft.go_to_node = instance.SCENE_LEFT
	$Navigation/GoRight.go_to_node = instance.SCENE_RIGHT
	Input.set_default_cursor_shape(Input.CURSOR_ARROW)
	
func _physics_process(delta):
	time_left = max(time_left - delta, 0)
	$UI/TimeLeft.text = str(int(time_left / 60)).lpad(2, "0") + ":" + str(int(time_left) % 60).lpad(2, "0")
	if time_left == 0:
		change_scene(Scenes.Scene.game_over)


func pearl_taken():
	const all_pearls_to_take = Commons.FIRST_ROUND_PEARLS + Commons.SECOND_ROUND_PEARLS
	Commons.score += 1
	$UI/Progress.text = str(Commons.score) + "/" + str(all_pearls_to_take)
	if Commons.score == Commons.FIRST_ROUND_PEARLS and not second_round_started:
		second_round_started = true
		print("Round two started...")
		for i in range(Commons.FIRST_ROUND_PEARLS, all_pearls_to_take):
			pearls[i].visible = true
			print("Pearl in " + str(pearls[i].get_parent().get_parent()))
	if Commons.score == all_pearls_to_take:
		change_scene(Scenes.Scene.game_over)

